'use strict'
import chai from 'chai'
import * as Stream from '../build/streams'

chai.should()

describe('Stream.unfold', () => {
  it('should create fibonacci series', () => {
    let uf_count = 0,
      max_uf_count = 10,
      fib_series = []

    for (let fib of Stream.unfold([0, 1], ([a, b]) => [a, [b, a+b]])) {
      if (uf_count >= max_uf_count) break
      uf_count++

      fib_series.push(fib)
    }

    fib_series.should.be.deep.equal([0,1,1,2,3,5,8,13,21,34])
  })
})

describe('Stream.cycle', () => {
  it('should cycle between 1,2,3,4,5', () => {
    let count = 0, max_count = 10, series = []
    for (let ele of Stream.cycle([1,2,3,4,5])) {
      if (count >= max_count) break
      count++

      series.push(ele)
    }

    series.should.be.deep.equal([1,2,3,4,5,1,2,3,4,5])
  })
})

describe('Stream.iterate', () => {
  it('should stream powers of 2', () => {
    let count = 0, max_count = 10, series = []
    for (let val of Stream.iterate(1, d => d * 2)) {
      if (count >= max_count) break
      count++

      series.push(val)
    }

    series.should.be.deep.equal([1,2,4,8,16,32,64,128,256,512])
  })
})

describe('Stream.repeatedly', () => {
  it('should stream 1s', () => {
    let count = 0, max_count = 10, series = []
    for (let val of Stream.repeatedly(function () { return 1 })) {
      if (count >= max_count) break
      count++

      series.push(val)
    }

    series.should.be.deep.equal([1,1,1,1,1,1,1,1,1,1])
  })
})