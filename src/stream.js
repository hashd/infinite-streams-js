function unfold(initial_state, func) {
  return {
    [Symbol.iterator]: function () {
      return {
        state: initial_state,
        next() {
          let [value, next_state] = func(this.state)
          this.state = next_state
          return { value, done: false }
        }
      }
    }
  }
}

function cycle(enumerable) {
  return {
    [Symbol.iterator]: function () {
      return {
        enumerable,
        done: true,
        iterable: null,
        next() {
          if (this.done === true) {
            this.iterable = this.enumerable[Symbol.iterator]()
          }
          let { value, done } = this.iterable.next()
          this.done = done

          return { value: (this.done === true ? this.next().value : value), done: false}
        }
      }
    }
  }
}

function iterate(initial_value, func) {
  return {
    [Symbol.iterator]: function () {
      return {
        value: initial_value,
        next() {
          let value = this.value
          this.value = func(value)

          return { value, done: false }
        }
      }
    }
  }
}

function repeatedly(func) {
  return {
    [Symbol.iterator]: function () {
      return {
        func: func,
        next() {
          let value = this.func()
          return { value, done: false }
        }
      }
    }
  }
}

export { unfold, cycle, iterate, repeatedly }